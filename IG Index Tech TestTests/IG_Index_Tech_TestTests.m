//
//  IG_Index_Tech_TestTests.m
//  IG Index Tech TestTests
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "TTUserModel.h"

@interface IG_Index_Tech_TestTests : XCTestCase

@property (nonatomic, strong) TTUserModel *userModel;

@end

@implementation IG_Index_Tech_TestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.userModel = [[TTUserModel alloc] init];
    
    self.userModel.country = @"uk";
    self.userModel.firstName = @"Ryan";
    self.userModel.lastName = @"Salton";
    self.userModel.email = @"ryansalton@google.com";
    self.userModel.phone = @"07890999999";
    self.userModel.dob = [NSDate dateWithTimeIntervalSince1970:0];
    
    self.userModel.timeAtAddress = @"5 years";
    self.userModel.annualSalary = @"40000";
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.userModel = nil;
    
    [super tearDown];

}

- (void)testCountryValid
{
    self.userModel.country = @"uk";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, text length is greater than zero");
}

- (void)testCountryNotValid
{
    self.userModel.country = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, text length is less than zero");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorCountry);
}

- (void)testFirstNameValid
{
    self.userModel.firstName = @"Ryan";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, text length is greater than zero");
}

- (void)testFirstNameNotValid
{
    self.userModel.firstName = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, text length is less than zero");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorFirstName);
}

- (void)testLastNameValid
{
    self.userModel.lastName = @"Salton";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, text length is greater than zero");
}

- (void)testLastNameNotValid
{
    self.userModel.lastName = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, text length is less than zero");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorLastName);
}

- (void)testDOBValid
{
    self.userModel.dob = [NSDate dateWithTimeIntervalSince1970:0];
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, age is greater than 18");
}

- (void)testDOBNotValid
{
    self.userModel.dob = [NSDate date];
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, age is less than 18");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorDOB);
}

- (void)testEmailValid
{
    self.userModel.email = @"ryan.salton0@googlemail.com";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, email address is valid");
}

- (void)testEmailNotValid
{
    self.userModel.email = @"ryan";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, email is not valid");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorEmail);
}

- (void)testPhoneValid
{
    self.userModel.phone = @"07890999999";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, phone number is valid");
}

- (void)testPhoneNotValid
{
    self.userModel.phone = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, phone number is not valid");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorPhone);
}

- (void)testSalaryValid
{
    self.userModel.country = @"german";
    self.userModel.annualSalary = @"123456";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, annual salary is valid");
}

- (void)testSalaryNotValid
{
    self.userModel.country = @"german";
    self.userModel.annualSalary = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, annual salary is not valid");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorAnnualSalary);
}

- (void)testTimeAtAddressValid
{
    self.userModel.country = @"us";
    self.userModel.timeAtAddress = @"5 years";
    
    XCTAssertTrue([self.userModel validate:nil], @"This should validate, time at address is valid");
}

- (void)testTimeAtAddressNotValid
{
    self.userModel.country = @"us";
    self.userModel.timeAtAddress = @"";
    
    NSError *error;
    XCTAssertFalse([self.userModel validate:&error], @"This should not validate, time at address is not valid");
    XCTAssertNotNil(error, @"This error should be populated");
    XCTAssertEqual(error.code, UserModelErrorTimeAtAddress);
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
