//
//  TTErrorLabel.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 13/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTErrorLabel.h"

@implementation TTErrorLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(8, 12, 8, 12))];
}

@end
