//
//  TTSwitchView.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 13/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTSwitchView.h"
#import "UIView+TurboTrader.h"


@implementation TTSwitchView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        _viewSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 4, 40, 30)];
        _viewSwitch.left = frame.size.width - _viewSwitch.width;
        [self addSubview:_viewSwitch];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width - _viewSwitch.width - 16, frame.size.height)];
        titleLabel.text = title;
        [titleLabel sizeToFit];
        titleLabel.top = (frame.size.height * 0.5) - (titleLabel.height * 0.5);
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
        [self addSubview:titleLabel];
        
        [titleLabel release];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
