//
//  TTDatePickerField.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTTextField.h"

@interface TTDatePickerField : TTTextField

@end
