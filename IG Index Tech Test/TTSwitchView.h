//
//  TTSwitchView.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 13/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSwitchView : UIView

@property (nonatomic, retain) UISwitch *viewSwitch;

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;

@end
