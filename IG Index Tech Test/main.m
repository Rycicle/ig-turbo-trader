//
//  main.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
