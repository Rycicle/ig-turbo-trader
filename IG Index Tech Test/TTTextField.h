//
//  TTTextField.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTTextField : UITextField

- (instancetype)initWithFrame:(CGRect)frame descText:(NSString *)descText;
- (void)setInvalid;
- (void)setValid;

@end
