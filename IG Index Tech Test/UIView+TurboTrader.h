//
//  UIView+TurboTrader.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TurboTrader)

@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;

@property (nonatomic, assign) CGPoint topLeftOrigin;
@property (nonatomic, assign) CGPoint topRightOrigin;
@property (nonatomic, assign) CGPoint bottomLeftOrigin;
@property (nonatomic, assign) CGPoint bottomRightOrigin;

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat halfWidth;
@property (nonatomic, assign) CGFloat halfHeight;

@property (nonatomic, assign, readonly) CGFloat maxContentWidth;

- (void)sizeToFitWithMinSize:(CGSize)minSize;
- (void)sizeToFitWithPadding:(CGFloat)padding;
- (void)sizeToFitWithMinWidth:(CGFloat)minWidth;
- (void)sizeToFitWithMinHeight:(CGFloat)minHeight;

- (void)centerInSuperview;
- (void)centerVerticallyInSuperview;
- (void)centerVerticallyInSuperviewResizingToSuperviewHeightWithPadding:(CGFloat)padding;
- (void)centerVerticallyInSuperviewWithLeftAlignedToLeftOfSuperviewWithPadding:(CGFloat)padding;
- (void)centerVerticallyInSuperviewWithRightAlignedToRightOfSuperviewWithPadding:(CGFloat)padding;
- (void)centerVerticallyInSuperviewWithLeftAlignedToRightOfView:(UIView *)view padding:(CGFloat)padding;
- (void)centerVerticallyInSuperviewWithRightAlignedToLeftOfView:(UIView *)view padding:(CGFloat)padding;
- (void)centerHorizontallyInSuperview;
- (void)centerHorizontallyInSuperviewResizingToSuperviewWidthWithPadding:(CGFloat)padding;
- (void)centerHorizontallyInSuperviewWithTopAlignedToTopOfSuperviewWithPadding:(CGFloat)padding;
- (void)centerHorizontallyInSuperviewWithBottomAlignedToBottomOfSuperviewWithPadding:(CGFloat)padding;
- (void)centerHorizontallyInSuperviewWithTopAlignedToBottomOfView:(UIView *)view padding:(CGFloat)padding;
- (void)centerHorizontallyInSuperviewWithBottomAlignedToTopOfView:(UIView *)view padding:(CGFloat)padding;
- (void)centerInView:(UIView *)view;
- (void)alignVerticalCenterWithView:(UIView *)view;
- (void)alignHorizontalCenterWithView:(UIView *)view;

- (void)alignTopToTopOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignBottomToBottomOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignRightToRightOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignLeftToLeftOfSuperviewWithPadding:(CGFloat)padding;

- (void)alignToTopLeftOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignToTopRightOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignToBottomLeftOfSuperviewWithPadding:(CGFloat)padding;
- (void)alignToBottomRightOfSuperviewWithPadding:(CGFloat)padding;

- (void)alignToTopLeftOfSuperviewWithPaddingVector:(CGVector)vector;
- (void)alignToTopRightOfSuperviewWithPaddingVector:(CGVector)vector;
- (void)alignToBottomLeftOfSuperviewWithPaddingVector:(CGVector)vector;
- (void)alignToBottomRightOfSuperviewWithPaddingVector:(CGVector)vector;

- (void)alignTopToTopOfView:(UIView *)view;
- (void)alignTopToBottomOfView:(UIView *)view padding:(CGFloat)padding;
- (void)alignBottomToBottomOfView:(UIView *)view;
- (void)alignBottomToTopOfView:(UIView *)view padding:(CGFloat)padding;
- (void)alignRightToRightOfView:(UIView *)view;
- (void)alignRightToLeftOfView:(UIView *)view padding:(CGFloat)padding;
- (void)alignLeftToLeftOfView:(UIView *)view;
- (void)alignLeftToRightOfView:(UIView *)view padding:(CGFloat)padding;

@end
