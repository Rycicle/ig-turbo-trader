//
//  ViewController.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTViewController.h"
#import "UIView+TurboTrader.h"
#import "TTTextField.h"
#import "TTDatePickerField.h"
#import "TTSwitchView.h"
#import "TTErrorLabel.h"
#import "TTUserModel.h"
#import "TTExtraFieldsView.h"
#import <iCarousel/iCarousel.h>

#define TEXTFIELD_MARGIN 24.0

@interface TTViewController () <iCarouselDataSource, iCarouselDelegate, UITextFieldDelegate>

@property (nonatomic, retain) TTUserModel *userModel;

@property (nonatomic, retain) NSMutableArray *countryArray;
@property (nonatomic, retain) iCarousel *carousel;
@property (nonatomic, retain) UIScrollView *scrollView;

@property (nonatomic, retain) NSMutableArray *textFieldArray;
@property (nonatomic, retain) TTTextField *nameFirstTextField;
@property (nonatomic, retain) TTTextField *nameLastTextField;
@property (nonatomic, retain) TTDatePickerField *dateOfBirthField;
@property (nonatomic, retain) TTTextField *emailTextField;
@property (nonatomic, retain) TTTextField *phoneTextField;

@property (nonatomic, retain) UIDatePicker *datePicker;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;

@property (nonatomic, retain) UIButton *submitButton;

@property (nonatomic, retain) NSMutableArray *extraFieldsArray;

@property (nonatomic, retain) TTErrorLabel *errorLabel;

// US Specific Fields
@property (nonatomic, retain) TTTextField *timeAtAddressTextField;
@property (nonatomic, retain) TTSwitchView *criminalRecordSwitchView;
@property (nonatomic, retain) TTSwitchView *usCitizenSwitchView;

// German Specific Fields
@property (nonatomic, retain) TTTextField *annualSalaryTextField;
@property (nonatomic, retain) TTSwitchView *outstandingDebtsSwitchView;

@end

@implementation TTViewController {
    NSInteger textFieldY;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:48.0/255.0 blue:18.0/255.0 alpha:1.0];
    
    // Add company logo
    
    UIImageView *igLogo = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]] autorelease];
    igLogo.transform = CGAffineTransformMakeScale(0.5, 0.5);
    igLogo.topLeftOrigin = CGPointMake(24, 24);
    [self.view addSubview:igLogo];
    
    // Available countries
    
    self.countryArray = [NSMutableArray arrayWithArray:@[@"uk", @"us", @"german"]];
    
    // Set default country to new model
    
    self.userModel.country = self.countryArray[0];
    
    // iCarousel
    
    self.carousel = [[[iCarousel alloc] initWithFrame:CGRectMake(0, igLogo.bottom, self.view.width, 100)] autorelease];
    self.carousel.type = iCarouselTypeRotary;

    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    
    [self.view addSubview:self.carousel];
    
    // Error handling label, hidden for now, will show when there is an erro
    
    self.errorLabel = [[[TTErrorLabel alloc] initWithFrame:CGRectMake(24, self.carousel.top + 20, self.view.width - 48, self.carousel.height - 20)] autorelease];
    self.errorLabel.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:183.0/255.0 alpha:1.0];
    self.errorLabel.layer.borderColor = [UIColor redColor].CGColor;
    self.errorLabel.layer.borderWidth = 3.0;
    self.errorLabel.text = @"THIS IS AN ERROR";
    self.errorLabel.textAlignment = NSTextAlignmentCenter;
    self.errorLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    self.errorLabel.textColor = [UIColor redColor];
    self.errorLabel.hidden = YES;
    self.errorLabel.numberOfLines = 0;
    self.errorLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.view addSubview:self.errorLabel];
    
    // Button to submit form
    
    self.submitButton = [[[UIButton alloc] initWithFrame:CGRectMake(self.view.width - 100, 12, 88, 50)] autorelease];
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    self.submitButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    [self.submitButton sizeToFit];
    self.submitButton.frame = CGRectMake(self.view.width - self.submitButton.width - 12, 12, self.submitButton.width, 50);
    [self.submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.submitButton addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.submitButton];
    
    // Form fields will go in here
    
    self.scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, self.carousel.bottom, self.view.width, self.view.height - self.carousel.bottom)] autorelease];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.alwaysBounceVertical = YES;
    [self.view addSubview:self.scrollView];
    
    // View to stop scrollview looking ugly when getting cut off at the top
    UIView *gradientView = [[UIView alloc] initWithFrame:CGRectMake(self.scrollView.left, self.scrollView.top, self.scrollView.width, 4)];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = gradientView.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[self.view.backgroundColor CGColor], (id)[[UIColor colorWithRed:216.0/255.0 green:48.0/255.0 blue:18.0/255.0 alpha:0.0] CGColor], nil];
    [gradientView.layer insertSublayer:gradient atIndex:0];
    
    [self.view addSubview:gradientView];
    
    [gradientView release];
    
    // Create the fields
    
    [self createFields];
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.width, ((UIView *)self.textFieldArray.lastObject).bottom + 24)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)createFields
{
    textFieldY = 8;
    
    // First and last names
    
    self.nameFirstTextField = [self newTextFieldAtY:textFieldY withLabel:@"First Name"];
    self.nameLastTextField = [self newTextFieldAtY:textFieldY withLabel:@"Last Name"];
    
    // DOB
    
    self.dateOfBirthField = [[[TTDatePickerField alloc] initWithFrame:CGRectMake(TEXTFIELD_MARGIN, textFieldY, self.view.width - (TEXTFIELD_MARGIN * 2), 40) descText:@"Date of Birth"] autorelease];
    self.dateOfBirthField.inputView = self.datePicker;
    
    [self.dateOfBirthField addTarget:self action:@selector(selectedDOB:) forControlEvents:UIControlEventEditingDidBegin];
    textFieldY += self.dateOfBirthField.height + 8;
    self.dateOfBirthField.delegate = self;
    [self.textFieldArray addObject:self.dateOfBirthField];
    [self.scrollView addSubview:self.dateOfBirthField];
    
    // Email
    
    self.emailTextField = [self newTextFieldAtY:textFieldY withLabel:@"Email"];
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    
    // Phone
    
    self.phoneTextField = [self newTextFieldAtY:textFieldY withLabel:@"Phone"];
    self.phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    
    // US Only fields
    
    self.timeAtAddressTextField = [self newTextFieldAtY:textFieldY withLabel:@"Time at Address"];
    self.usCitizenSwitchView = [[[TTSwitchView alloc] initWithFrame:CGRectMake(TEXTFIELD_MARGIN, 0, self.view.width - (TEXTFIELD_MARGIN * 2), 40) title:@"US Citizen"] autorelease];
    self.criminalRecordSwitchView = [[[TTSwitchView alloc] initWithFrame:CGRectMake(TEXTFIELD_MARGIN, 0, self.view.width - (TEXTFIELD_MARGIN * 2), 40) title:@"Criminal Record"] autorelease];
    
    [self.textFieldArray addObjectsFromArray:@[self.usCitizenSwitchView, self.criminalRecordSwitchView]];
    
    // German only fields
    
    self.annualSalaryTextField = [self newTextFieldAtY:textFieldY withLabel:@"Annual Salary"];
    self.annualSalaryTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    self.outstandingDebtsSwitchView = [[[TTSwitchView alloc] initWithFrame:CGRectMake(TEXTFIELD_MARGIN, 0, self.view.width - (TEXTFIELD_MARGIN * 2), 40) title:@"Any Outstanding Debts"] autorelease];
    
    [self.textFieldArray addObject: self.outstandingDebtsSwitchView];
    
    // Create extra fields views
    
    TTExtraFieldsView *ukExtraFields = [[TTExtraFieldsView alloc] initWithFrame:CGRectMake(0, self.phoneTextField.bottom + 24, self.view.width, 100) title:@"" andFields:@[]];
    TTExtraFieldsView *usExtraFields = [[TTExtraFieldsView alloc] initWithFrame:ukExtraFields.frame title:@"Background Information" andFields:@[self.timeAtAddressTextField, self.criminalRecordSwitchView, self.usCitizenSwitchView]];
    TTExtraFieldsView *geExtraFields = [[TTExtraFieldsView alloc] initWithFrame:ukExtraFields.frame title:@"Financial Status" andFields:@[self.annualSalaryTextField, self.outstandingDebtsSwitchView]];
    
    ukExtraFields.hidden = NO;
    
    [self.scrollView addSubview:ukExtraFields];
    [self.scrollView addSubview:usExtraFields];
    [self.scrollView addSubview:geExtraFields];
    
    [self.extraFieldsArray addObject:ukExtraFields];
    [self.extraFieldsArray addObject:usExtraFields];
    [self.extraFieldsArray addObject:geExtraFields];
    
    [ukExtraFields release];
    [usExtraFields release];
    [geExtraFields release];
}

- (TTTextField *)newTextFieldAtY:(NSInteger)posY withLabel:(NSString *)label
{
    TTTextField *newField = [[[TTTextField alloc] initWithFrame:CGRectMake(TEXTFIELD_MARGIN, posY, self.view.width - (TEXTFIELD_MARGIN * 2), 40) descText:label] autorelease];
    textFieldY += newField.height + 8;
    newField.delegate = self;
    [self.textFieldArray addObject:newField];
    [self.scrollView addSubview:newField];
    
    return newField;
}

- (UIDatePicker *)datePicker
{
    // CURRENT MEMORY LEAK: iOS platform bug http://stackoverflow.com/questions/12830278/uidatepicker-leaks
    
    if (_datePicker == nil)
    {
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 100)];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        [_datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        
        NSDate *currentDate = [NSDate date];
        
        NSCalendar *gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] autorelease];
        NSDateComponents *offsetComponents = [[[NSDateComponents alloc] init] autorelease];
        [offsetComponents setYear:-18];
        NSDate *maximumDate = [gregorian dateByAddingComponents:offsetComponents toDate:currentDate options:0];
        
        _datePicker.maximumDate = maximumDate;
    }
    
    return _datePicker;
}

- (NSMutableArray *)textFieldArray
{
    if (_textFieldArray == nil)
    {
        _textFieldArray = [[NSMutableArray array] retain];
    }
    
    return _textFieldArray;
}

- (NSMutableArray *)extraFieldsArray
{
    if (_extraFieldsArray == nil)
    {
        _extraFieldsArray = [[NSMutableArray array] retain];
    }
    
    return _extraFieldsArray;
}

- (NSDateFormatter *)dateFormatter
{
    if (_dateFormatter == nil)
    {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    
    return _dateFormatter;
}

- (TTUserModel *)userModel
{
    if (_userModel == nil)
    {
        _userModel = [[TTUserModel alloc] init];
    }
    
    return _userModel;
}

- (void)selectedDOB:(id)selector
{
    self.dateOfBirthField.text = [self.dateFormatter stringFromDate:self.datePicker.date];
    self.userModel.dob = self.datePicker.date;
}

- (void)dateChanged:(UIDatePicker *)selector
{
    self.dateOfBirthField.text = [self.dateFormatter stringFromDate:selector.date];
    self.userModel.dob = selector.date;
}

- (void)submitAction
{
    self.userModel.firstName = self.nameFirstTextField.text;
    self.userModel.lastName = self.nameLastTextField.text;
    self.userModel.email = self.emailTextField.text;
    self.userModel.phone = self.phoneTextField.text;
    
    self.userModel.timeAtAddress = self.timeAtAddressTextField.text;
    self.userModel.criminalRecord = self.criminalRecordSwitchView.viewSwitch.on;
    self.userModel.usCitizen = self.usCitizenSwitchView.viewSwitch.on;
    
    self.userModel.annualSalary = self.annualSalaryTextField.text;
    self.userModel.outstandingDebts = self.outstandingDebtsSwitchView.viewSwitch.on;
    
    NSError *error;
    if (![self.userModel validate:&error])
    {
        NSString *errorMessage = @"Please fill out all required fields";
        
        switch (error.code) {
            case UserModelErrorDOB:
                errorMessage = @"Invalid Date of Birth, You must be at least 18 years of age";
                [self.dateOfBirthField setInvalid];
                break;
            
            case UserModelErrorEmail:
                errorMessage = @"Invalid Email Address";
                [self.emailTextField setInvalid];
                break;
            
            case UserModelErrorPhone:
                [self.phoneTextField setInvalid];
                break;
                
            case UserModelErrorFirstName:
                [self.nameFirstTextField setInvalid];
                break;
                
            case UserModelErrorLastName:
                [self.nameLastTextField setInvalid];
                break;
                
            case UserModelErrorAnnualSalary:
                errorMessage = @"Please supply an annual salary";
                [self.annualSalaryTextField setInvalid];
                break;
                
            case UserModelErrorTimeAtAddress:
                errorMessage = @"You must enter your time at address";
                [self.timeAtAddressTextField setInvalid];
                break;
                
            default:
                break;
        }
        
        [self showErrorWithMessage:errorMessage];
        
        return;
    }
    
    // THIS IS WHERE THE API CALL WOULD GO TO THE SERVER IN DEVELOPMENT

}

- (void)showErrorWithMessage:(NSString *)message
{
    self.errorLabel.text = message;
    
    self.errorLabel.alpha = 0.0;
    self.errorLabel.hidden = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.errorLabel.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 delay:2.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.errorLabel.alpha = 0.0;
        } completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
    
    [_userModel release];
    [_scrollView release];
    [_carousel release];
    [_countryArray release];
    [_errorLabel release];
    [_datePicker release];
    [_dateFormatter release];
    [_submitButton release];
    
    for (UITextField *textfield in _textFieldArray)
    {
        [textfield release];
    }
    
    [_textFieldArray release];
    [_extraFieldsArray release];
    
    self.carousel = nil;
    
    [super dealloc];
}

- (TTTextField *)getFirstResponder
{
    for (TTTextField *textField in self.textFieldArray)
    {
        if ([textField isFirstResponder])
        {
            return textField;
        }
    }
    
    return nil;
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, keyboardFrameBeginRect.size.height, 0)];
    
    TTTextField *firstResponder = [self getFirstResponder];
    
    if (firstResponder)
    {
        [self.scrollView scrollRectToVisible:firstResponder.frame animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
}


#pragma mark - iCarousel methods

- (void)carouselWillBeginDragging:(iCarousel *)carousel
{
    for (TTTextField *textfield in self.textFieldArray)
    {
        if (textfield.isFirstResponder)
        {
            [textfield resignFirstResponder];
        }
    }
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    self.userModel.country = self.countryArray[carousel.currentItemIndex];
    
    TTExtraFieldsView *currentView = self.extraFieldsArray[carousel.currentItemIndex];
    
    for (TTExtraFieldsView *view in self.extraFieldsArray)
    {
        if (view != currentView)
        {
            view.hidden = YES;
        }
    }
    
    currentView.hidden = NO;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.phoneTextField.bottom + currentView.height + 32);
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return self.countryArray.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (view == nil)
    {
        view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 50)] autorelease];
        UIImageView *flag = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"flag-%@.png", self.countryArray[index]]]] autorelease];
        flag.left = 6;
        
        [view addSubview:flag];
    }
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionFadeMin:
            return -0.2;
        case iCarouselOptionFadeMax:
            return 0.2;
        case iCarouselOptionFadeRange:
            return 2.0;
        case iCarouselOptionArc:
            return M_PI * 2.0;
        case iCarouselOptionSpacing:
            return 3;
        default:
            return value;
    }
}

#pragma mark - Textfield delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

@end
