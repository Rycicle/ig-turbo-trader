//
//  TTErrorLabel.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 13/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTErrorLabel : UILabel

@end
