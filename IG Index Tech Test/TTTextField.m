//
//  TTTextField.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTTextField.h"
#import "UIView+TurboTrader.h"

#define LABEL_WIDTH 120

@interface TTTextField()

@property (nonatomic, retain) UILabel *descLabel;

@end

@implementation TTTextField


- (instancetype)initWithFrame:(CGRect)frame descText:(NSString *)descText
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor grayColor].CGColor;
        self.layer.borderWidth = 1.0;
        self.layer.cornerRadius = 2.0;
        
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, LABEL_WIDTH - 10, frame.size.height)];
        _descLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
        _descLabel.text = descText;
        [self addSubview:_descLabel];
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 410, 320, 44)];
        [toolbar setBarStyle:UIBarStyleDefault];
        [toolbar sizeToFit];
        
        UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignFirstResponder)];
        [toolbar setItems:@[flexButton, doneButton]];
        
        [flexButton release];
        [doneButton release];
        
        [self setInputAccessoryView:toolbar];
        
        [toolbar release];
    }
    
    return self;
}

- (void)dealloc
{
    [_descLabel release];
    
    [super dealloc];
}

- (BOOL)becomeFirstResponder
{
    [super becomeFirstResponder];
    [self setValid];
    
    return YES;
}

- (void)setValid
{
    self.backgroundColor = [UIColor whiteColor];
    self.descLabel.textColor = [UIColor blackColor];
    self.layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)setInvalid
{
    self.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:191.0/255.0 blue:189.0/255.0 alpha:1.0];
    self.descLabel.textColor = [UIColor redColor];
    self.layer.borderColor = [UIColor redColor].CGColor;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    int margin = 8;
    CGRect inset = CGRectMake(bounds.origin.x + LABEL_WIDTH, bounds.origin.y, (bounds.size.width - LABEL_WIDTH) - margin, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    int margin = 8;
    CGRect inset = CGRectMake(bounds.origin.x + LABEL_WIDTH, bounds.origin.y, (bounds.size.width - LABEL_WIDTH) - margin, bounds.size.height);
    return inset;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
