//
//  ExtraFieldsView.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTExtraFieldsView : UIView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title andFields:(NSArray *)fields;

@end
