//
//  TTUserModel.h
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UserModelErrorCountry,
    UserModelErrorFirstName,
    UserModelErrorLastName,
    UserModelErrorDOB,
    UserModelErrorEmail,
    UserModelErrorPhone,
    UserModelErrorTimeAtAddress,
    UserModelErrorAnnualSalary
} UserModelError;

@interface TTUserModel : NSObject

@property (nonatomic, retain) NSString *country;

@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSDate *dob;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *phone;

// German specific

@property (nonatomic, retain) NSString *annualSalary;
@property (nonatomic, assign) BOOL outstandingDebts;

// US specific

@property (nonatomic, retain) NSString *timeAtAddress;
@property (nonatomic, assign) BOOL criminalRecord;
@property (nonatomic, assign) BOOL usCitizen;

- (BOOL)validate:(NSError **)error;

@end
