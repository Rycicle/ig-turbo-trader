//
//  TTUserModel.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTUserModel.h"

@implementation TTUserModel

- (BOOL)validate:(NSError **)error
{
    if (self.country.length == 0)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorCountry userInfo:nil];
        }
        return NO;
    }
    
    if (self.firstName.length == 0)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorFirstName userInfo:nil];
        }
        return NO;
    }
    
    if (self.lastName.length == 0)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorLastName userInfo:nil];
        }
        return NO;
    }
    
    NSDate *checkDate = [NSDate date];
    
    NSCalendar *gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] autorelease];
    NSDateComponents *offsetComponents = [[[NSDateComponents alloc] init] autorelease];
    [offsetComponents setYear:-18];
    checkDate = [gregorian dateByAddingComponents:offsetComponents toDate:checkDate options:0];
    
    if (self.dob == nil || [self.dob compare:checkDate] == NSOrderedDescending)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorDOB userInfo:nil];
        }
        return NO;
    }
    
    if (self.email.length == 0 || ![self NSStringIsValidEmail:self.email])
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorEmail userInfo:nil];
        }
        return NO;
    }
    
    if (self.phone.length == 0)
    {
        if (error != NULL)
        {
            *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorPhone userInfo:nil];
        }
        return NO;
    }
    
    
    if ([self.country isEqualToString:@"us"])
    {
        if (self.timeAtAddress.length == 0)
        {
            if (error != NULL)
            {
                *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorTimeAtAddress userInfo:nil];
            }
            return NO;
        }
    }
    else if ([self.country isEqualToString:@"german"])
    {
        if (self.annualSalary.length == 0)
        {
            if (error != NULL)
            {
                *error = [NSError errorWithDomain:NSStringFromClass([self class]) code:UserModelErrorAnnualSalary userInfo:nil];
            }
            return NO;
        }
    }
    
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
