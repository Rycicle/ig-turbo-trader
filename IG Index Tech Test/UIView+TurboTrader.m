//
//  UIView+TurboTrader.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "UIView+TurboTrader.h"

@implementation UIView (TurboTrader)

#pragma mark - Size & Position

@dynamic halfWidth,halfHeight;

- (CGFloat)sanitizePosition:(CGFloat)initialPos
{
    return roundf(initialPos);
}

- (CGPoint)origin
{
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGFloat)xPosAndWidth
{
    CGFloat output = [self x] + [self width];
    return output;
}

- (CGFloat)yPosAndHeight
{
    CGFloat output = [self y] + [self height];
    return output;
}

- (void)setPosition:(CGPoint)position
{
    CGRect rect = [self frame];
    rect.origin = position;
    [self setFrame:rect];
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (void)setCenterXView:(UIView *)view
{
    CGRect frame = view.frame;
    self.x = frame.size.width * 0.5 - self.width * 0.5;
}

- (void)setX:(CGFloat)x
{
    CGRect rect = [self frame];
    rect.origin.x = x;
    [self setFrame:rect];
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setY:(CGFloat)y
{
    CGRect rect = [self frame];
    rect.origin.y = y;
    [self setFrame:rect];
}

- (CGFloat)left
{
    return self.x;
}

- (void)setLeft:(CGFloat)left
{
    self.x = left;
}

- (CGFloat)right
{
    return self.x + self.width;
}

- (void)setRight:(CGFloat)right
{
    self.x = right - self.width;
}

- (CGFloat)top
{
    return self.y;
}

- (void)setTop:(CGFloat)top
{
    self.y = top;
}

- (CGFloat)bottom
{
    return self.y + self.height;
}

- (void)setBottom:(CGFloat)bottom
{
    self.y = bottom - self.height;
}

- (CGPoint)topLeftOrigin
{
    return self.origin;
}

- (void)setTopLeftOrigin:(CGPoint)topLeftOrigin
{
    self.position = topLeftOrigin;
}

- (CGPoint)topRightOrigin
{
    CGPoint point = self.origin;
    point.x = self.right;
    
    return point;
}

- (void)setTopRightOrigin:(CGPoint)topRightOrigin
{
    self.top = topRightOrigin.y;
    self.right = topRightOrigin.x;
}

- (CGPoint)bottomLeftOrigin
{
    CGPoint point;
    point.x = self.left;
    point.y = self.bottom;
    
    return point;
}

- (void)setBottomLeftOrigin:(CGPoint)bottomLeftOrigin
{
    self.left = bottomLeftOrigin.x;
    self.bottom = bottomLeftOrigin.y;
}

- (CGPoint)bottomRightOrigin
{
    CGPoint point;
    point.x = self.right;
    point.y = self.bottom;
    
    return point;
}

- (void)setBottomRightOrigin:(CGPoint)bottomRightOrigin
{
    self.right = bottomRightOrigin.x;
    self.bottom = bottomRightOrigin.y;
}

- (CGFloat)width
{
    return self.bounds.size.width;
}

- (void)setWidth:(CGFloat)width
{
    CGRect rect = [self frame];
    rect.size.width = width;
    [self setFrame:rect];
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.size.height = height;
    [self setFrame:rect];
}

- (CGFloat)halfWidth
{
    return [self width] * 0.5;
}

- (CGFloat)halfHeight
{
    return [self height] * 0.5;
}

- (CGFloat)maxContentWidth
{
    return self.width - (10.0 * 2);
}

#pragma mark -

- (void)sizeToFitWithMinSize:(CGSize)minSize
{
    [self sizeToFit];
    
    if (self.size.width < minSize.width)
    {
        self.width = minSize.width;
    }
    
    if (self.size.height < minSize.height)
    {
        self.height = minSize.height;
    }
}

- (void)sizeToFitWithPadding:(CGFloat)padding
{
    [self sizeToFit];
    
    self.width += padding;
    self.height += padding;
}

- (void)sizeToFitWithMinWidth:(CGFloat)minWidth
{
    [self sizeToFit];
    
    if (self.width < minWidth)
    {
        self.width = minWidth;
    }
}

- (void)sizeToFitWithMinHeight:(CGFloat)minHeight
{
    [self sizeToFit];
    
    if (self.frame.size.height < minHeight)
    {
        self.height = minHeight;
    }
}

#pragma mark -

- (void)centerInSuperview
{
    if (self.superview)
    {
        [self centerHorizontallyInSuperview];
        [self centerVerticallyInSuperview];
    }
}

- (void)centerVerticallyInSuperview
{
    if (self.superview)
    {
        self.y = (self.superview.bounds.size.height - self.height) / 2.0;
    }
}

- (void)centerVerticallyInSuperviewResizingToSuperviewHeightWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.height = self.superview.bounds.size.height - (padding * 2);
        [self centerVerticallyInSuperview];
    }
}

- (void)centerVerticallyInSuperviewWithLeftAlignedToLeftOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        [self centerVerticallyInSuperview];
        [self alignLeftToLeftOfSuperviewWithPadding:padding];
    }
}

- (void)centerVerticallyInSuperviewWithRightAlignedToRightOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        [self centerVerticallyInSuperview];
        [self alignRightToRightOfSuperviewWithPadding:padding];
    }
}

- (void)centerVerticallyInSuperviewWithLeftAlignedToRightOfView:(UIView *)view padding:(CGFloat)padding
{
    if (self.superview && view)
    {
        [self centerVerticallyInSuperview];
        [self alignLeftToRightOfView:view
                             padding:padding];
    }
}

- (void)centerVerticallyInSuperviewWithRightAlignedToLeftOfView:(UIView *)view padding:(CGFloat)padding
{
    if (self.superview && view)
    {
        [self centerVerticallyInSuperview];
        [self alignRightToLeftOfView:view
                             padding:padding];
    }
}

- (void)centerHorizontallyInSuperview
{
    if (self.superview)
    {
        self.x = (self.superview.bounds.size.width - self.width) / 2.0;
    }
}

- (void)centerHorizontallyInSuperviewResizingToSuperviewWidthWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.width = self.superview.bounds.size.width - (padding * 2);
        [self centerHorizontallyInSuperview];
    }
}

- (void)centerHorizontallyInSuperviewWithTopAlignedToTopOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        [self centerHorizontallyInSuperview];
        [self alignTopToTopOfSuperviewWithPadding:padding];
    }
}

- (void)centerHorizontallyInSuperviewWithBottomAlignedToBottomOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        [self centerHorizontallyInSuperview];
        [self alignBottomToBottomOfSuperviewWithPadding:padding];
    }
}

- (void)centerHorizontallyInSuperviewWithTopAlignedToBottomOfView:(UIView *)view padding:(CGFloat)padding
{
    if (self.superview && view)
    {
        [self centerHorizontallyInSuperview];
        [self alignTopToBottomOfView:view
                             padding:padding];
    }
}

- (void)centerHorizontallyInSuperviewWithBottomAlignedToTopOfView:(UIView *)view padding:(CGFloat)padding
{
    if (self.superview && view)
    {
        [self centerHorizontallyInSuperview];
        [self alignBottomToTopOfView:view
                             padding:padding];
    }
}

- (void)centerInView:(UIView *)view
{
    if (view)
    {
        self.x = (view.width - self.width) / 2.0;
        self.y = (view.height - self.height) / 2.0;
    }
}

- (void)alignVerticalCenterWithView:(UIView *)view
{
    if (view)
    {
        self.y = view.y + ((view.height - self.height) / 2.0);
    }
}

- (void)alignHorizontalCenterWithView:(UIView *)view
{
    if (view)
    {
        self.x = view.x + ((view.width - self.width) / 2.0);
    }
}

#pragma mark - View Alignment In Superview

- (void)alignTopToTopOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.top = padding;
    }
}

- (void)alignBottomToBottomOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.bottom = self.superview.bounds.size.height - padding;
    }
}

- (void)alignRightToRightOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.right = self.superview.bounds.size.width - padding;
    }
}

- (void)alignLeftToLeftOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.left = padding;
    }
}

#pragma mark - View Corner Alignment In Superview

- (void)alignToTopLeftOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.topLeftOrigin = CGPointMake(padding, padding);
    }
}

- (void)alignToTopRightOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.topRightOrigin = CGPointMake(self.superview.bounds.size.width - padding, padding);
    }
}

- (void)alignToBottomLeftOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.bottomLeftOrigin = CGPointMake(padding, self.superview.bounds.size.height - padding);
    }
}

- (void)alignToBottomRightOfSuperviewWithPadding:(CGFloat)padding
{
    if (self.superview)
    {
        self.bottomRightOrigin = CGPointMake(self.superview.bounds.size.width - padding, self.superview.bounds.size.height - padding);
    }
}

- (void)alignToTopLeftOfSuperviewWithPaddingVector:(CGVector)vector
{
    if (self.superview)
    {
        self.topLeftOrigin = CGPointMake(vector.dx, vector.dy);
    }
}

- (void)alignToTopRightOfSuperviewWithPaddingVector:(CGVector)vector
{
    if (self.superview)
    {
        self.topRightOrigin = CGPointMake(self.superview.bounds.size.width - vector.dx, vector.dy);
    }
}

- (void)alignToBottomLeftOfSuperviewWithPaddingVector:(CGVector)vector
{
    if (self.superview)
    {
        self.bottomLeftOrigin = CGPointMake(vector.dx, self.superview.bounds.size.height - vector.dy);
    }
}

- (void)alignToBottomRightOfSuperviewWithPaddingVector:(CGVector)vector
{
    if (self.superview)
    {
        self.bottomRightOrigin = CGPointMake(self.superview.bounds.size.width - vector.dx, self.superview.bounds.size.height - vector.dy);
    }
}

#pragma mark - View Alignment Relative To Other View

- (void)alignTopToTopOfView:(UIView *)view
{
    if (view)
    {
        self.top = view.top;
    }
}

- (void)alignTopToBottomOfView:(UIView *)view padding:(CGFloat)padding
{
    if (view)
    {
        self.top = view.bottom + padding;
    }
}

- (void)alignBottomToBottomOfView:(UIView *)view
{
    if (view)
    {
        self.bottom = view.bottom;
    }
}

- (void)alignBottomToTopOfView:(UIView *)view padding:(CGFloat)padding
{
    if (view)
    {
        self.bottom = view.top - padding;
    }
}

- (void)alignRightToRightOfView:(UIView *)view
{
    if (view)
    {
        self.right = view.right;
    }
}

- (void)alignRightToLeftOfView:(UIView *)view padding:(CGFloat)padding
{
    if (view)
    {
        self.right = view.left - padding;
    }
}

- (void)alignLeftToLeftOfView:(UIView *)view
{
    if (view)
    {
        self.left = view.left;
    }
}

- (void)alignLeftToRightOfView:(UIView *)view padding:(CGFloat)padding
{
    if (view)
    {
        self.left = view.right + padding;
    }
}

@end
