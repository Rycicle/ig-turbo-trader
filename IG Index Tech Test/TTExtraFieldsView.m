//
//  ExtraFieldsView.m
//  IG Index Tech Test
//
//  Created by Ryan Salton on 12/05/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTExtraFieldsView.h"
#import "UIView+TurboTrader.h"
#import "TTTextField.h"


@implementation TTExtraFieldsView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title andFields:(NSArray *)fields
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.hidden = YES;
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(24, 0, self.width, 30)] autorelease];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.text = title;
        [self addSubview:titleLabel];
        
        NSInteger posY = titleLabel.bottom + 8;
        
        for (UIView *field in fields)
        {
            field.top = posY;
            [self addSubview:field];
            posY += field.height + 8;
        }
        
        self.height = posY;
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
